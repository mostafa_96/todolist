import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HomeComponent } from './components/home/home.component';
import { TaskComponent } from './components/home/task/task.component';
import { StarterComponent } from './components/starter/starter.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicAuthHtppInterceptorService } from './services/basic-auth-htpp-interceptor-service.service';
import { DialogBodyComponent } from './components/home/dialog-body/dialog-body.component';
import { DatePipe } from '@angular/common';
import { AddManagerDialogComponent } from './components/home/add-manager-dialog/add-manager-dialog.component';
import { ConfirmDeleteDialogComponent } from './components/home/confirm-delete-dialog/confirm-delete-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TaskComponent,
    StarterComponent,
    SignupComponent,
    LoginComponent,
    DialogBodyComponent,
    AddManagerDialogComponent,
    ConfirmDeleteDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHtppInterceptorService,
      multi: true
  },
  DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
