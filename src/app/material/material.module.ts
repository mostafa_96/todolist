import { NgModule } from '@angular/core';
import { MatButtonModule} from '@angular/material/button';
import { MatSliderModule } from '@angular/material/slider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSnackBarModule} from '@angular/material/snack-bar';



const MATERIALCOMPONENTS = [MatButtonModule,MatSliderModule,MatToolbarModule
  ,MatSidenavModule,MatCardModule,MatCheckboxModule,MatButtonToggleModule,
  MatInputModule,MatIconModule,MatListModule,MatSelectModule,MatDialogModule,
  MatDatepickerModule,MatNativeDateModule,MatMenuModule,MatPaginatorModule,
  MatSnackBarModule
  
];

@NgModule({
  imports: [
    MATERIALCOMPONENTS
  ],
  exports:[
    MATERIALCOMPONENTS
  ]
})

export class MaterialModule { }
