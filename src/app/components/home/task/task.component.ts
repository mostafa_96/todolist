import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Task } from 'src/app/models/task.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { ConfirmDeleteDialogComponent } from '../confirm-delete-dialog/confirm-delete-dialog.component';
import { DialogBodyComponent } from '../dialog-body/dialog-body.component';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  @Input() task:Task;
  users:User[];
  loggedInUser: User;
  isManager: boolean;
  constructor(private taskService:TaskService,private matDialog: MatDialog,
    private datepipe: DatePipe,private snackBar: MatSnackBar,
    private userService:UserService,private authServie: AuthService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getLoggedInUser(){
    this.authServie.getLoggedInUser().subscribe(
      (user) => {
        this.loggedInUser = user;
        this.isManager = (this.loggedInUser.mgr_id == null);
      });
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.height = '500px';
    dialogConfig.data = this.task;
    let dialogRef = this.matDialog.open(DialogBodyComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((task) => {
      if (!(task == null)){
          this.updateTask(task);        
      }
    })
  }

  openDeleteDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '400px';
    dialogConfig.height = '200px';
    let dialogRef = this.matDialog.open(ConfirmDeleteDialogComponent,dialogConfig);
    dialogRef.afterClosed().subscribe(
      (action:string)=>{
        if(action == 'ok'){
          this.deleteTask();
        }
      }
    )
  }

  deleteTask(){

    this.taskService.deleteTask(this.task.id).subscribe(
      (response)=>{
        this.taskService.taskChangeEmitter.next(this.task);
        let snackBarRef = this.taskService.openSnackBar('task deleted successfully','ok');
      }
    )
    
  }

  archiveTask(){
    this.task.state_id = 3;
    this.task.completed_date = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.taskService.updateTask(this.task).subscribe(
      (data)=>{
        this.taskService.taskChangeEmitter.next(this.task);
      }
    )
  }

  completeTask(){
    this.task.state_id = 2;
    this.task.completed_date = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.taskService.updateTask(this.task).subscribe(
      (data)=>{
        this.taskService.taskChangeEmitter.next(this.task);
      }
    )
  }

  updateTask(task:Task){
    this.taskService.updateTask(task).subscribe(
      (data)=>{
        this.taskService.taskChangeEmitter.next(this.task);
        let snackBarRef = this.taskService.openSnackBar('task updated successfully','ok');
      }
    )
  }

  getColor(){
    if(this.task.state_id == 1){
      return '#00FF7F';
    }else if(this.task.state_id == 2){
      return '#F0FFFF';
    }else{
      return '#DCDCDC';
    }
  }

  getUsers(){
    this.userService.getUsers().subscribe(
      (users)=>{
        this.users = users;
        console.log(this.users);
      }
    );
  }
  
}
