import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Task } from 'src/app/models/task.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { AddManagerDialogComponent } from './add-manager-dialog/add-manager-dialog.component';
import { DialogBodyComponent } from './dialog-body/dialog-body.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tasks: Task[] = [];
  name: string = '';
  loggedInUser: User;
  state_id: number = 0;
  title: string = 'All Tasks';
  isManager: boolean;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  current_page:number = 1;
  to:number;
  total:number;
  lastTask = 0;

  in_current_page:number = 1;
  in_to:number;
  in_total:number;
  in_lastTask = 0;

  com_current_page:number = 1;
  com_to:number;
  com_total:number;
  com_lastTask = 0;

  arc_current_page:number = 1;
  arc_to:number;
  arc_total:number;
  arc_lastTask = 0;

  constructor(private taskService: TaskService, private authServie: AuthService
    , private matDialog: MatDialog, private snackBar: MatSnackBar
    ,private userService:UserService) {
  }

  ngOnInit(): void {
    this.taskService.openSnackBar('welcome to todo list','ok');
    this.authServie.getLoggedInUser().subscribe(
      (user) => {
        this.loggedInUser = user;
        this.name = this.loggedInUser.name;
        this.isManager = (this.loggedInUser.mgr_id == null);
        //this.getAllTasks();
        this.loadTasks(1);
      });
    this.listenToTaskChange();
  }


  loadTasks(page:number){
    this.state_id=0;
    this.taskService.loadTasks(this.loggedInUser.id,page).subscribe(
      (response:any)=>{
        this.current_page = response.current_page;
        this.total = response.total;
        this.to = response.to;
        this.tasks = response.data;
        console.log(this.current_page," ",this.total," ",this.to);
      }
    )
  }

  getTasksByState(page:number) {
    this.taskService.getTasksByState(this.loggedInUser.id, this.state_id,page).subscribe(
      (response:any) => {
        this.tasks = response.data;
        this.setPagesData(response.current_page,response.to,response.total);
      }
    )
  }

  getCurrnetPage(){
    let cur = 1;
    if(this.state_id == 1){
      cur = this.in_current_page;
    }else if(this.state_id == 2){
      cur = this.com_current_page;
    }else if(this.state_id == 3){
      cur = this.arc_current_page;
    }
    return cur;
  }

  setPagesData(cur:number,to:number ,total:number){
    if(this.state_id == 1){
      this.in_current_page = cur;
      this.in_to = to;
      this.in_total = total;
    }else if(this.state_id == 2){
      this.com_current_page = cur;
      this.com_to = to;
      this.com_total = total;
    }
    else if(this.state_id == 3){
      this.arc_current_page = cur;
      this.arc_to = to;
      this.arc_total = total;
    }
  }


  openDialog(action: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.height = '500px';
    dialogConfig.data = action;
    let dialogRef = this.matDialog.open(DialogBodyComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((task) => {
      if (!(task == null)) {
        this.addTask(task);
      }
    })
  }

  openAddManagerDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.height = '500px';
    let dialogRef = this.matDialog.open(AddManagerDialogComponent, dialogConfig);
  }

  addManager(manager:User){
    this.userService.addManager(manager).subscribe(
      (response)=>{
        this.taskService.openSnackBar('a new manager is added successfully','ok');
        console.log(response);
      },(err)=>{
        let errors = err.error.errors;
        this.handleError(errors);
      }
    );
  }

  handleError(errors:any){
    if(errors.name){
      this.taskService.openSnackBar(errors.name[0],'ok');
    }else if(errors.email){
      this.taskService.openSnackBar(errors.email[0],'ok');
    }
    else if(errors.password){
      this.taskService.openSnackBar(errors.password[0],'ok');
    }
    else if(errors.password_confirmation){
      this.taskService.openSnackBar(errors.password_confirmation[0],'ok');
    }
  }

  addTask(task: Task) {
    this.taskService.addTask(task).subscribe(
      (newTask) => {
        this.taskService.openSnackBar('task created successfully', 'ok');
        if (this.state_id == 0) {
          this.loadTasks(this.current_page);
        } else {
          this.getTasksByState(this.getCurrnetPage());
        }
      }
    )
  }

  listenToTaskChange() {
    this.taskService.taskChangeEmitter.subscribe(
      (task) => {
        if (this.state_id == 0) {
          this.loadTasks(this.current_page);
        } else {
          this.getTasksByState(this.getCurrnetPage());
        }
      }
    )
  }

  switchState(state_id: number) {
    this.state_id = state_id;
    if (state_id == 0) {
      this.title = 'All Tasks';
    } else if (state_id == 1) {
      this.title = 'In Progress';
    } else if (state_id == 2) {
      this.title = 'Completed Tasks';
    } else if (state_id == 3) {
      this.title = 'Archived Tasks';
    }
    if(state_id == 0){
      this.loadTasks(this.current_page);
    }else{
      this.getTasksByState(this.getCurrnetPage());
    }
    
  }

  logout() {
    this.authServie.logOut();
  }


}
