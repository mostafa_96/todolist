import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Priority } from 'src/app/models/priority.model';
import { Task } from 'src/app/models/task.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { PriorityService } from 'src/app/services/priority.service';
import {
  MAT_DIALOG_DATA
} from "@angular/material/dialog";
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dialog-body',
  templateUrl: './dialog-body.component.html',
  styleUrls: ['./dialog-body.component.css']
})
export class DialogBodyComponent implements OnInit {

  taskForm:FormGroup;
  priorities:Priority[];
  task:Task = null;
  users:User[];
  loggedInUser:User;
  action = 'update';
  constructor(private priorityService:PriorityService,
    public dialogRef: MatDialogRef<DialogBodyComponent>,
    private authService:AuthService ,private datepipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: Task,
    private userService:UserService) { }

  ngOnInit(): void {
    if(typeof this.data == 'object') {
      this.task = this.data;
    }else{
      this.action = this.data
    }
    this.authService.getLoggedInUser().subscribe(
      (user)=>{
        this.loggedInUser = user;
      }
    ) 
    this.taskForm = new FormGroup({
      'title':new FormControl(this.task == null?'':this.task.title,Validators.required),
      'description':new FormControl(this.task == null?'':this.task.description,[Validators.required]),
      'priority':new FormControl(this.task == null?'':this.task.priority_id,Validators.required),
      'due_date':new FormControl(this.task == null?'':this.task.due_date,Validators.required) ,
      'user_id':new FormControl(this.task == null?'':this.task.user_id)      
    });

    this.getPriorities();

    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe(
      (users)=>{
        this.users = users;
      }
    )
  }

  getPriorities(){
    this.priorityService.getPriorities().subscribe(
      (priorities)=>{
        this.priorities = priorities;
      }
    )
  }

  save(){
    if(this.taskForm.invalid) return;
    this.task = new Task();
    this.task.title = this.taskForm.get('title').value;
    this.task.description = this.taskForm.get('description').value;
    //////
    let date = this.taskForm.get('due_date').value;
    this.task.due_date = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.task.created_date = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    //////
    this.task.priority_id = this.taskForm.get('priority').value;
    this.task.state_id = 1;
    this.task.created_by = this.loggedInUser.id;
    if(this.action == 'assign'){
      this.task.user_id = this.taskForm.get('user_id').value;
    }else{
      this.task.user_id = this.loggedInUser.id;
    }

    this.dialogRef.close(this.task);
  }

  updateTask(){
    if(this.taskForm.invalid) return;
    this.task.title = this.taskForm.get('title').value;
    this.task.description = this.taskForm.get('description').value;
    //////
    let date = this.taskForm.get('due_date').value;
    this.task.due_date = this.datepipe.transform(date, 'yyyy-MM-dd');
    this.task.created_date = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    //////
    this.task.priority_id = this.taskForm.get('priority').value;
    console.log("update task",this.task);
    this.dialogRef.close(this.task);
  }

  close(){
    this.dialogRef.close(null);
  }

}
