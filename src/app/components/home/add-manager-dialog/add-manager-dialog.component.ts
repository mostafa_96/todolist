import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/models/user.model';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-manager-dialog',
  templateUrl: './add-manager-dialog.component.html',
  styleUrls: ['./add-manager-dialog.component.css']
})
export class AddManagerDialogComponent implements OnInit {

  managerForm:FormGroup;
  manager:User;
  constructor(public dialogRef: MatDialogRef<AddManagerDialogComponent>,
    private userService:UserService,private taskService:TaskService) { }

  ngOnInit(): void {
    this.managerForm = new FormGroup({
      'name':new FormControl('',Validators.required),
      'email':new FormControl('',[Validators.required]),
      'password':new FormControl('',Validators.required),
      'password_confirmation':new FormControl('',Validators.required) ,
    });
  }

  save(){
    if(this.managerForm.invalid) {
      console.log('invalid');
      return;
    }
    this.manager = new User();
    this.manager.name = this.managerForm.get('name').value;
    this.manager.email = this.managerForm.get('email').value;
    this.manager.password = this.managerForm.get('password').value;
    this.manager.password_confirmation = this.managerForm.get('password_confirmation').value;
    this.addManager(this.manager);
  }

  addManager(manager:User){
    this.userService.addManager(manager).subscribe(
      (response)=>{
        this.taskService.openSnackBar('a new manager is added successfully','ok');
        this.dialogRef.close();
        console.log(response);
      },(err)=>{
        let errors = err.error.errors;
        this.handleError(errors);
      }
    );
  }

  handleError(errors:any){
    if(errors.name){
      this.taskService.openSnackBar(errors.name[0],'ok');
    }else if(errors.email){
      this.taskService.openSnackBar(errors.email[0],'ok');
    }
    else if(errors.password){
      this.taskService.openSnackBar(errors.password[0],'ok');
    }
    else if(errors.password_confirmation){
      this.taskService.openSnackBar(errors.password_confirmation[0],'ok');
    }
  }

  getErrorMessage() {
    if (this.managerForm.get('email').hasError('required')) {
      return "email field is required";
    }
    return this.managerForm.get('email').hasError('email') ? 'Not a valid email' : '';
  }


  close(){
    this.dialogRef.close(null);
  }

}
