import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  loginForm: FormGroup;
  error: string;

  constructor(private loginservice: AuthService, private router: Router,
    private snackBar: MatSnackBar,private taskService:TaskService) { }

  ngOnInit(): void {

    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required)
    });
  }

  login() {

    if (this.loginForm.invalid) {
      this.taskService.openSnackBar('enter missing data','ok');
      return;
    }

    let email = this.loginForm.get('email').value;
    let password = this.loginForm.get('password').value;
    this.loginservice.login(email, password)
      .subscribe((data: any) => {
        localStorage.setItem("email", email);
        let tokenStr = "Bearer " + data.Authorization;
        localStorage.setItem("token", tokenStr);
        this.router.navigate(['/home']);
        this.loginservice.autoLogout(24*60*60*1000);
      },
        (err) => {
          if (err.error.error) {
            this.taskService.openSnackBar(err.error.error, 'ok');
          } else {
            let errors = err.error.errors;
            this.handleError(errors);
          }
        }
      );
  }

  handleError(errors: any) {
    if (errors.email) {
      this.taskService.openSnackBar(errors.email[0], 'ok');
    } else if (errors.password) {
      this.taskService.openSnackBar(errors.password[0], 'ok');
    }
  }

  getErrorMessage() {
    if (this.loginForm.get('email').hasError('required')) {
      return 'You must enter a value';
    }
    return this.loginForm.get('email').hasError('email') ? 'Not a valid email' : '';
  }

}
