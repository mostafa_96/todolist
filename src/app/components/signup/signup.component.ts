import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  user: User;
  managers: User[];
  invalid:boolean = false;

  constructor(private authService: AuthService, private userService: UserService,
    private router: Router,private taskService:TaskService) { }

  ngOnInit(): void {

    this.signUpForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required),
      'password_confirmation': new FormControl('', Validators.required),
      'mgr_id': new FormControl('', Validators.required)
    });

    this.getManagers();
  }

  getErrorMessage() {
    if (this.signUpForm.get('email').hasError('required')) {
      return "email field is required";
    }
    return this.signUpForm.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  getManagers() {
    this.userService.getAllManagers().subscribe(
      (managers) => {
        this.managers = managers;
      }
    )
  }

  setUser(){
    this.user = new User();
    this.user.name = this.signUpForm.get('name').value;
    this.user.email = this.signUpForm.get('email').value;
    this.user.password = this.signUpForm.get('password').value;
    this.user.password_confirmation = this.signUpForm.get('password_confirmation').value;
    this.user.mgr_id = this.signUpForm.get('mgr_id').value;
  }

  signUp() {
    if (this.signUpForm.invalid) {
      this.taskService.openSnackBar('enter missing data','ok');
      return ;
    }
    
    this.setUser();

    this.authService.register(this.user).subscribe((response) => {
        this.authService.login(this.user.email, this.user.password).subscribe((data) => {
          localStorage.setItem("email", this.user.email);
          let tokenStr = "Bearer " + data.Authorization;
          localStorage.setItem("token", tokenStr);
          this.router.navigate(['/home']);
        });
      }, err => {
        let errors = err.error.errors;
        this.handleError(errors);
      })
  }

  handleError(errors:any){
    if(errors.name){
      this.taskService.openSnackBar(errors.name[0],'ok');
    }else if(errors.email){
      this.taskService.openSnackBar(errors.email[0],'ok');
    }
    else if(errors.password){
      this.taskService.openSnackBar(errors.password[0],'ok');
    }
    else if(errors.password_confirmation){
      this.taskService.openSnackBar(errors.password_confirmation[0],'ok');
    }
    else if(errors.mgr_id){
      this.taskService.openSnackBar(errors.mgr_id[0],'ok');
    }
  }

}
