export class Task{
    id?:number;
    title:string;
    description:string;
    created_date:string;
    due_date:string;
    completed_date?:string;
    user_id:number;
    state_id:number;
    priority_id:number;
    created_by:number;

}