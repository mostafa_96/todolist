import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { TaskService } from './task.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url: string = 'http://127.0.0.1:8000/api/auth/';
  tokenExpirationTimer:any;

  constructor(private httpClient: HttpClient,private router:Router,
    private taskService:TaskService) { }

  login(email: string, password: string) {
    return this.httpClient.post<any>(this.url + 'login', { email: email, password: password });
  }

  register(user:User){
    return this.httpClient.post<any>(this.url + 'register' , user);
  }

  isUserLoggedIn() {
    let user = localStorage.getItem("email");
    return !(user === null);
  }

  logOut() {
    return this.httpClient.post(this.url+'logout','').subscribe(
      (response:any)=>{
      localStorage.removeItem("email");
      this.taskService.openSnackBar(response.message,'ok');
      this.router.navigate(['/app']);
      if(this.tokenExpirationTimer){
        clearTimeout(this.tokenExpirationTimer);
      }
      this.tokenExpirationTimer = null;
    })
  }

  autoLogout(expirationDuration:number){
    this.tokenExpirationTimer = setTimeout(
      ()=>{
        this.logOut()
      },expirationDuration
    )

  }

  getLoggedInUser(){
    return this.httpClient.get<User>(this.url+'me');
  }

}
