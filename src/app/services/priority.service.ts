import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Priority } from '../models/priority.model';

@Injectable({
  providedIn: 'root'
})
export class PriorityService {

  url:string = 'http://127.0.0.1:8000/api/priorities/';
  constructor(private httpClient:HttpClient) { }

  getPriorities(){
    return this.httpClient.get<Priority[]>(this.url);
  }
}
