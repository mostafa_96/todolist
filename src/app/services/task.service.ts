import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  url:string = 'http://127.0.0.1:8000/api/tasks/';
  taskChangeEmitter = new Subject<Task>();


  constructor(private httpClient: HttpClient,
    private snackBar: MatSnackBar) { }

  public addTask(task:Task){
    return this.httpClient.post<Task>(this.url,task);
  }

  public deleteTask(task_id:number){
    return this.httpClient.delete(this.url+task_id);
  }

  public getTasksByState(user_id:number,state_id:number,page:number){
    return this.httpClient.get<Task[]>(this.url+ 'bystate/'+user_id+'/'+state_id+'?page='+page);
  }

  public updateTask(task:Task){
    return this.httpClient.put(this.url+task.id,task);
  }

  public loadTasks(user_id:number,pageNumber:number){
    return this.httpClient.get(this.url+'all/'+user_id+'?page='+pageNumber);
  }

  openSnackBar(message: string, action: string) {
    return this.snackBar.open(message, action, {
      duration: 3000,
      verticalPosition: 'top'
    });
  }

}
