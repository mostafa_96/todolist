import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { State } from '../models/state.model';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  url:string = 'http://127.0.0.1:8000/api/states/';

  constructor(private httpClient :HttpClient) { }

  getAllStates(){
    return this.httpClient.get<State>(this.url);
  }
}
