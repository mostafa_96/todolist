import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url:string = 'http://127.0.0.1:8000/api/users/';

  constructor(private httpClient :HttpClient) { }

  getAllUsers(){
    return this.httpClient.get<User[]>(this.url);
  }

  getAllManagers(){
    return this.httpClient.get<User[]>(this.url+'managers'); 
  }

  getUsers(){
    return this.httpClient.get<User[]>(this.url+'users'); 
  }

  addManager(manager:User){
    return this.httpClient.post(this.url,manager);
  }
  

}
