import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'to-do-list';
  loggedIn:boolean = false;

  constructor(private router:Router,private authService:AuthService){
    if(this.authService.isUserLoggedIn()){
      this.router.navigate(['/home']);
    }else{
      this.router.navigate(['/app']);
    }
  }
}
